//
//  MapViewController.swift
//  NearestStores
//
//  Created by Tragvar on 2/26/16.
//  Copyright © 2016 Tragvar. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreData


class MapViewController: UIViewController, GMSMapViewDelegate {

    var mapView: GMSMapView!
    let locationManager = CLLocationManager()
    let kAPIKey: String = "AIzaSyCqy-qQUsuw09Gkx-ua_i-CfGj6sZbCi3k"
    var type: String = ""
    var radius = 2000;

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        let camera = GMSCameraPosition.cameraWithLatitude(46.965922, longitude:32.002992, zoom:13)
        mapView = GMSMapView.mapWithFrame(CGRectZero, camera:camera)
        mapView.delegate = self
        mapView.myLocationEnabled = true
        mapView.settings.myLocationButton = true
        mapView.settings.compassButton = true
        self.view = mapView
        self.title = "Nearests \(type)"
        self.fetchPlacesRadius(radius, types: type) { (_: [Place]) -> Void in
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchPlacesRadius(rad: Int, types: String, completion: (([Place]) -> Void)) -> () {
        let lat:String = "46.965922" //String(format:"%f", locationManager.location!.coordinate.latitude)
        let lon:String = "32.002992" //String(format:"%f", locationManager.location!.coordinate.longitude)

        let urlString = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\(lat),\(lon)&radius=\(rad)&types=\(types)&key=\(kAPIKey)"
        
        let url = NSURL(string: urlString)
        let request = NSURLRequest(URL: url!)
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config)
                
        let task = session.dataTaskWithRequest(request) {
            (
            let data, let response, let error) in
            
            guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
                print("error")
                return
            }

            do {
                if let jsonResult = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
                    
                    if let results: NSArray = jsonResult["results"] as? NSArray{
                        dispatch_async(dispatch_get_main_queue(), {
                            if results.count > 0{
                                for index in 0...results.count - 1 {
                                    let item = results[index] as? [String: AnyObject]
                                    self.savePlaceWith(item!)
                                    self.showMarkerWith(item!)
                                }
                            } else {
                                let alert = UIAlertController(title: "Warning", message: "Is no objects in \(rad) m.", preferredStyle: UIAlertControllerStyle.Alert)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                                self.presentViewController(alert, animated: true, completion: nil)
                            }
                        })
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        task.resume()
    }
    
    func savePlaceWith(dictionary: [String: AnyObject]) {

        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let entity =  NSEntityDescription.entityForName("Place", inManagedObjectContext:managedContext)
        let place = Place(entity: entity!, insertIntoManagedObjectContext: managedContext)
        place.name = dictionary["name"] as? String
        place.lat = dictionary["geometry"]!["location"]!!["lat"] as? String
        place.lng = dictionary["geometry"]!["location"]!!["lng"] as? String
        place.vicinity = dictionary["vicinity"] as? String
        do {
            try managedContext.save()
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    func showMarkerWith(dictionary: [String: AnyObject]) {
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake((dictionary["geometry"]!["location"]!!["lat"] as? Double)!, (dictionary["geometry"]!["location"]!!["lng"] as? Double)!)
        marker.title = dictionary["name"] as? String
        marker.snippet = dictionary["vicinity"] as? String
        marker.appearAnimation = kGMSMarkerAnimationPop
        marker.map = self.mapView
    }
}



extension MapViewController: CLLocationManagerDelegate {

    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .AuthorizedWhenInUse {
            locationManager.startUpdatingLocation()

            mapView.myLocationEnabled = true
            mapView.settings.myLocationButton = true
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            locationManager.stopUpdatingLocation()
        }
        
    }
}

