//
//  ViewController.swift
//  NearestStores
//
//  Created by Tragvar on 2/26/16.
//  Copyright © 2016 Tragvar. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var radiusSlider: UISlider!
    @IBOutlet weak var radiusLabel: UILabel!

    
    var typesList:[String] = ["police", "hospital", "bank", "food", "cemetery", "embassy", "store", "bar", "museum", "car_rental"]
    var radius = 2000
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        self.title = "Types List"
        
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = bgImageView.bounds
        blurEffectView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight] // for supporting device rotation
        self.bgImageView.addSubview(blurEffectView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return typesList.count ?? 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell")! as UITableViewCell
        cell.textLabel!.text = self.typesList[indexPath.row]
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        performSegueWithIdentifier("MapSegue", sender: indexPath)
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if (segue.identifier == "MapSegue") {
            let mapVC:MapViewController = segue.destinationViewController as! MapViewController
            let indexPath = self.tableView.indexPathForSelectedRow
            mapVC.type = self.typesList[indexPath!.row]
            mapVC.radius = self.radius
        }
    }
    
    @IBAction func sliderValueChenged(sender: UISlider) {
        let currentValue = Int(sender.value)
        self.radius = currentValue
        self.radiusLabel.text = "Now is \(currentValue)m."
    }
}

